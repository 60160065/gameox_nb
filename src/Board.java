public class Board {
	private char[][]table = {
            {'-','-','-'},
            {'-','-','-'},
            {'-','-','-'}};
	private Player X;
	private Player O;
	private Player winner;
	private Player currentPlayer;
	private int turnCount;
	
	public Board(Player X , Player O) {
		this.X = X;
		this.O = O;
		currentPlayer = X;
		winner = null;
	
	}
	public char[][]getTable(){
		return table;
	}
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	public boolean setTable(int row , int col) {
		if (table[row][col]=='-') {
			table[row][col]=currentPlayer.getName();
			return true;
		}
		return false;
	}
	private boolean checkRow(int row) {
		for(int col=0;col<table[row].length;col++) {
			if (table[row][col]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkRow() {
		if (checkRow(0)||checkRow(1)||checkRow(2)) {
			return true;
		}
		return false;
	}
	private boolean checkRCol(int col) {
		for(int row=0;col<table.length;row++) {
			if (table[row][col]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkCol() {
		if (checkRow(0)||checkRow(1)||checkRow(2)) {
			return true;
		}
		return false;
	}
	private boolean checkx1() {
		for(int i=0 ;i<table.length;i++) {
			if (table[i][i]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkx2() {
		for(int i=0 ;i<table.length;i++) {
			if (table[2-i][i]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkDraw() {
		if(turnCount==8) {
			X.draw();
			O.draw();
			return true;
		}
		return false;
	}
	public boolean checkWin() {
		if (checkRow() || checkCol()||checkx1()||checkx2()) {
			winner = currentPlayer;
			if(currentPlayer==X) {
				X.win();
				O.lose();
			}else {
				O.win();
				X.lose();
			}
			return true;
		}
		return false;
	}
	public Player getWinner() {
		return winner;
	}
	public boolean isFinish() {
		if (checkWin()) {
			
			return true;
		}
		if (checkDraw()) {
			return true;
		}
		return false;
	}
	public void switchPlayer() {
		if (currentPlayer==X) {
			currentPlayer=O;
		}else {
			currentPlayer=X;
		}
		turnCount++;
	}
	
	
	

}
